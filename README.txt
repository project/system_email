********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: System Email Module
Author: Robert Castelo
Drupal: 5.x
********************************************************************
DESCRIPTION:

Provide HTML version of system email messages (welcome, password reminder).



********************************************************************
INSTALLATION:

Note: It is assumed that you have Drupal up and running.  Be sure to
check the Drupal web site if you need assistance.

1. Place the entire module directory into your Drupal directory:
   sites/all/modules/
   

2. Enable the module by navigating to:

   administer > build > modules
     
  Click the 'Save configuration' button at the bottom to commit your
  changes. 


  
  
********************************************************************
USAGE     



********************************************************************
AUTHOR CONTACT

- Comission New Features:
   http://www.codepositive.com/contact


        
********************************************************************
ACKNOWLEDGEMENT

Initial development sponsored by BrightTalk <http://www.brighttalk.com>