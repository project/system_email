<?php

/**
 * Implementation of hook_form_alter().
 */
function system_email_form_alter($form_id, &$form) {

	switch ($form_id) {
		case 'user_admin_settings':
			system_email_html_fields($form);
			break;
		case 'user_register':
			// password stored at validate stage because hook_mail_alter() runs before submit
			$form['#validate']['system_email_password_entered'] = array(); 
			break;
	}
}

/**
 * Modify the User Settings form.
 */
function system_email_html_fields(&$form) {

	$form_old = $form['email'];
	// list of plain text fields to add a HTML version off
	$text_fields = array('user_mail_welcome_body', 'user_mail_admin_body', 'user_mail_approval_body', 'user_mail_pass_body');

	foreach ($form_old as $key => $value) {
		$form_new[$key] = $value;
		// add html field after the plain text version
		if (in_array($key, $text_fields)) {
			$default_value = variable_get($key . '_html', $value['#default_value']);
			$form_new[$key . '_html'] = system_email_add_field($value['#title'], $value['#description'], $default_value);
		}
	}
	
	$form['email'] = $form_new;
}

/**
 * Add a HTML version of a plain text field.
 */
function system_email_add_field($title, $description, $default_value) {
	$field = array(
		'#type' => 'textarea',
		'#title' => check_plain("$title (HTML)"),
		'#description' => check_plain($description),
		'#default_value' => $default_value,
		'#cols' => 60,
		'#rows' => 15,
	);
 
	return $field;
}

/**
 * Password has been entered and will be preserved for other functions to use.
 */
function system_email_password_entered($form_id, $form_values) {
	system_email_password_store($form_values['pass'], TRUE); 
}

/**
 * Implementation of hook_mail_alter().
 */
function system_email_mail_alter(&$mailkey, &$to, &$subject, &$body, &$from, &$headers) {
	// messages with HTML version
	$html_fields = array('user-register-welcome' => 'welcome_body', 
											 'user-register-notify' => 'admin_body', 
											 'user-register-approval-admin' => 'approval_body', 
											 'user_mail_pass' => 'pass_body'); 
											
  $message_id = $html_fields[$mailkey];																														
  if (empty($message_id)) return;

  // check if user has opted for plain text or HTML email
  $user = user_load(array('mail' => $to));

	// abstract out html selection option
	$email_format = module_invoke_all('system_email_format', $user);
	
	if ($email_format == 'plain_text') return;
	 
	// get message template
	$default_value = _user_mail_text($message_id);
	$body = variable_get('user_mail_' . $message_id . '_html', $default_value);
	
	// replace tokens
	$variables = system_email_token_variables($user, $mail, $message_id);
  $body = strtr($body, $variables);

	// include HTML headers
	$headers['Content-Type'] = 'text/html; charset=UTF-8';
}

/**
 * Generate array of variables to replace tokens in email.
 */
function system_email_token_variables($user, $mail, $message_id) {

	global $base_url;	
 
	// replace tokens
	$variables = array('!username' => $user->name, 
										 '!site' => variable_get('site_name', 'Drupal'), 
										 '!uri' => $base_url, 
										 '!uri_brief' => substr($base_url, strlen('http://')), 
										 '!mailto' => $mail, 
										 '!date' => format_date(time()), 
										 '!login_uri' => url('user', NULL, NULL, TRUE), 
										 '!edit_uri' => url('user/'. $user->uid .'/edit', NULL, NULL, TRUE),
										 '!login_url' => user_pass_reset_url($user),
										);
										
	if ($message_id == 'welcome_body' || $message_id == 'approval_body') {
    $pass = user_password();
    user_save($user, array('pass' => $pass));
		$variables['!password'] = $pass; 
	}
	elseif ($message_id == 'admin_body') {
		$variables['!password'] = system_email_password_store(); 
	}								
										
	return $variables;
}

/**
 * Store password entered to static variable for use by other functions.
 */
function system_email_password_store($pass = NULL, $reset = FALSE) {
	static $pass_store;
	
	if (empty($pass_store) || $reset) {
		$pass_store = $pass;
	}

	return $pass_store;
}

